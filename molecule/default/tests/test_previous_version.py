import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("previous_version")


JAVA_VERSION = "1.8.0_151"


def test_java_version(host):
    cmd = host.command("java -version 2>&1")
    assert 'java version "{}"'.format(JAVA_VERSION) in cmd.stdout


def test_java_home(host):
    cmd = host.command(r'su -l -c "echo \$JAVA_HOME"')
    assert cmd.stdout.strip() == "/usr/java/jdk{}".format(JAVA_VERSION)
