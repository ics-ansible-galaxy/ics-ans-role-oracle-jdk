---
- name: install java
  yum:
    name: "jdk1.8-{{ oracle_jdk_version }}"
    state: present
    allow_downgrade: true

- name: look for the java home directory
  find:
    paths: "/usr/java"
    patterns: 'jdk{{ oracle_jdk_version }}*'
    file_type: directory
  register: find_java_home

- name: set java_home variable
  set_fact:
    java_home: "{{ find_java_home.files[0].path }}"

- name: create java_env.sh file
  template:
    src: java_env.sh.j2
    dest: /etc/profile.d/java_env.sh
    owner: root
    group: root
    mode: 0644

- name: set installed java version as default
  alternatives:
    name: "{{ item.exe }}"
    link: "/usr/bin/{{ item.exe }}"
    path: "{{ item.path }}/{{ item.exe }}"
  with_items:
    - path: "{{ java_home }}/jre/bin"
      exe: "java"
    - path: "{{ java_home }}/jre/bin"
      exe: "keytool"
    - path: "{{ java_home }}/bin"
      exe: "javac"
    - path: "{{ java_home }}/bin"
      exe: "javadoc"

- name: uncomment the crypto.policy setting
  replace:
    path: "{{ java_home }}/jre/lib/security/java.security"
    regexp: "^#crypto.policy=.*"
    replace: "crypto.policy=unlimited"

# If the above commented line didn't exist, this ensures
# that the line is added to the file
- name: set the crypto.policy to unlimited
  lineinfile:
    state: present
    path: "{{ java_home }}/jre/lib/security/java.security"
    regexp: "^crypto.policy="
    line: "crypto.policy=unlimited"

- name: configure the JVM to use /dev/urandom instead of /dev/random
  lineinfile:
    state: present
    path: "{{ java_home }}/jre/lib/security/java.security"
    regexp: "^securerandom.source="
    line: "securerandom.source=file:/dev/urandom"

- name: add VisualVM link to the desktop menu
  template:
    src: jvisualvm.desktop.j2
    dest: /usr/share/applications/jvisualvm.desktop
    owner: root
    group: root
    mode: 0644
  when: oracle_jdk_visualvm_link
